Problem: 
	- metabolic networks are reasonibly large 
	- same metabolic networks do not necessarily have the same annotations (e.g. metabolits can have different anotations)
	- different granulations of metabolic networks (some networks include low level molecules)
	- species to species translations (find KREBS cycle)

Alignment:
	- align a subgraph to whole graph

Embedding:
	- embed nodes of a graph 
	- can be used for visualization and alignment
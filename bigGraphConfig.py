def get_torchbiggraph_config():

    config = dict(
        # I/O data
        edge_paths=[],
        entity_path=".",
        checkpoint_path=".",  

        # Graph structure
        entities={
            'metabolite_id': {'num_partitions': 1},
			'reaction_id': {'num_partitions': 1},
        },
        relations=[{
            'name': 'consumes',
            'lhs': 'metabolite_id',
            'rhs': 'reaction_id',
            'operator': 'none',
        },
		{
            'name': 'produces',
            'lhs': 'reaction_id',
            'rhs': 'metabolite_id', 
            'operator': 'none',
        }],

        # Scoring model
        dimension=128,               
        global_emb=False,   

        # Training
        num_epochs=128,    
        lr=0.01,  

        # Misc
        hogwild_delay=1,
        comparator = "dot",      
        loss_fn = "softmax"             
    ) 

    return config   


    
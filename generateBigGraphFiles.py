import os 
import random 

def convert_path(fname): 
    basename, _ = os.path.splitext(fname)
    out_dir = basename + '_partitioned'
    return out_dir

def random_split_file(fpath,  filenames, train_fraction): 
    root = os.path.dirname(fpath)

    output_paths = [
        os.path.join(root, filenames['train']),
        os.path.join(root, filenames['test']),
    ]

    #If files already exists    
    if all(os.path.exists(path) for path in output_paths):
        print("Found some files that indicate that the input data "
              "has already been shuffled and split, not doing it again.")
        print("These files are: %s" % ", ".join(output_paths))
        return

    print('Shuffling and splitting train/test file. This may take a while.')
    train_file = os.path.join(root, filenames['train'])
    test_file = os.path.join(root, filenames['test'])

    print('Reading data from file: ', fpath)
    with open(fpath, "rt") as in_tf:    
        lines = in_tf.readlines()

    # The first few lines are comments
    lines = lines[1:]
    print('Shuffling data')
    random.shuffle(lines)
    split_len = int(len(lines) * train_fraction)

    print('Splitting to train and test files') 
    with open(train_file, "wt") as out_tf_train:
        for line in lines[:split_len]:
            
            out_tf_train.write(line)

    with open(test_file, "wt") as out_tf_test:
        for line in lines[split_len:]:
            out_tf_test.write(line)


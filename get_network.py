import requests  
import json   
import os.path  
import numpy as np 
import math 
import networkx as nx  
import pickle   

class Model: 

    def __init__(self, biGG_model_path="http://bigg.ucsd.edu/api/v2/models", biGG_model_name="Recon3D"):
        self.biGG_model_path = biGG_model_path 
        self.biGG_model_name = biGG_model_name 
        self.biGG_model = None  
        self.metabolite_numbers = None 
        self.reaction_numbers = None         
        self.biGG_path = os.path.join(".", "models", "biGG", biGG_model_name)
        self.biGG_path_file = os.path.join(self.biGG_path , self.biGG_model_name + ".json")  
        self.adj_matrix = None 
        self.adj_react_matrix = None    
        self.graph_x = None    
        self.reaction_names = None   


    def getBiGGModelDict(self):
        jsonContent = "" 
        #if file already exists  
        if os.path.isfile(self.biGG_path_file):  
            #read file 
            print("Reading file ... ") 
            with open(self.biGG_path_file) as biGG_file:
                jsonContent = json.load(biGG_file)  
        else:    
            #get whole model as json string
            print("Making http request ... ")  
            httpRequest = requests.get(self.biGG_model_path + "/" + self.biGG_model_name + "/download")   
            jsonContent = httpRequest.json()   

            print("Writting to file ... ")  
            os.makedirs(os.path.dirname(self.biGG_path_file), exist_ok=True) 
            with open(self.biGG_path_file, "w") as biGG_file:
                json.dump(jsonContent, biGG_file) 
        #save dictionary to biGG_model 
        self.biGG_model = jsonContent   

        self.number_metabolites = len(self.biGG_model["metabolites"])
        self.number_reactions = len(self.biGG_model["reactions"])  

        return self.biGG_model 

    def getReactionMetaboliteNumbers(self): 
        if self.metabolite_numbers == None and self.reaction_numbers == None:            
            model = self.biGG_model   
            reactions = model["reactions"] 
            metabolites = model["metabolites"]  
            metab_ind = 0     
            react_ind = 0 
            metabolite_numbers = {}  
            reaction_numbers = {}
            reaction_names = {}  

            #traverse all metabolites and construct metabolites to matrix mapping  
            for metabolite in metabolites:
                metabolite_numbers[metabolite["id"]] = metab_ind 
                metab_ind += 1  

            react_ind = metab_ind

            for reaction in reactions:
                reaction_numbers[reaction["id"]] = react_ind
                reaction_names[str(react_ind)] = reaction["name"]   
                react_ind += 1  

            self.metabolite_numbers = metabolite_numbers  
            self.reaction_numbers = reaction_numbers   
            self.reaction_names = reaction_names                  

    def getBigGraphNotation(self):
        self.getReactionMetaboliteNumbers() 
        connections = [] 
        model = self.biGG_model   
        reactions = model["reactions"]  
        for reaction in reactions: 

            for metabolite, num in reaction["metabolites"].items():    
                #update matrix      
                if num >= 0: 
                    #reaction -> metabolite 
                    connections.extend([[str(self.reaction_numbers[reaction["id"]]), "produces", str(self.metabolite_numbers[metabolite])]]) 
                else: 
                    #metabolite -> reaction   
                    connections.extend([[str(self.metabolite_numbers[metabolite]), "consumes", str(self.reaction_numbers[reaction["id"]])]])      
        return connections


    #generates one big graph file for two models; useful for training joined embeddings 
    def generateJoinedBigGraphFile(self, model1, model2): 

        fname = os.path.join(self.biGG_path, self.biGG_model_name + ".tsv")     
        if not os.path.isfile(fname): 

            conn1 = model1.getBigGraphNotation()    
            conn2 = model2.getBigGraphNotation()

            total = model1.number_metabolites + model1.number_reactions 
            conn2 = [[str(int(el[0]) + total), el[1], str(int(el[2]) + total)] for el in conn2]       
            conn1.extend(conn2)  

            connections = conn1

            dirname = os.path.dirname(fname) 
            if not os.path.exists(dirname):
                os.makedirs(dirname) 
            with open(fname, 'a') as tsv_file: 
                np.savetxt(tsv_file, connections, delimiter="\t",  fmt="%s") 

    #generate a tab delimited file suited for pytorch biggraph embeddings
    def generateBigGraphFile(self):  
        fname = os.path.join(self.biGG_path, self.biGG_model_name + ".tsv")

        #if the file does not exists 
        if not os.path.isfile(fname):
            connections = self.getBigGraphNotation()  
            connections.insert(0, ["#source_id", "relation", "drain_id"])           
            #write to file 
            dirname = os.path.dirname(fname) 
            if not os.path.exists(dirname):
                os.makedirs(dirname)             
            with open(fname, 'wt') as tsv_file: 
                np.savetxt(tsv_file, connections, delimiter="\t",  fmt="%s")     

    #returns the adjacency matrix of bipartite graph   
    #nodes are metabolites and reactions 
    #metabolites are inputs and oputs of reaction
    def getAdjacencyMatrix(self):
        if self.adj_matrix is None:
            self.getReactionMetaboliteNumbers()

            model = self.biGG_model 

            print("Constructing matrix") 
            reactions = model["reactions"]
            metabolites = model["metabolites"]   

            matrix_size = self.number_metabolites + self.number_reactions  
            adj_matrix = np.zeros((matrix_size, matrix_size))  

            #traverse all reactions
            for reaction in reactions:
                for metabolite, num in reaction["metabolites"].items():   
                    #update matrix  
                    #check if reaction is reversible (negative flux) 
                    if reaction["lower_bound"] < 0:
                        adj_matrix[self.reaction_numbers[reaction["id"]], self.metabolite_numbers[metabolite]] += 1 
                        adj_matrix[self.metabolite_numbers[metabolite], self.reaction_numbers[reaction["id"]]] += 1     
                    else:
                        #set only directed edges  
                        if num >= 0: 
                            #reaction -> metabolite
                            adj_matrix[self.reaction_numbers[reaction["id"]], self.metabolite_numbers[metabolite]] += 1    
                        else: 
                            #metabolite -> reaction  
                            adj_matrix[self.metabolite_numbers[metabolite], self.reaction_numbers[reaction["id"]]] += 1     
            print("Adjacency matrix constructed") 
            self.adj_matrix = adj_matrix
        return self.adj_matrix     

    def getReactionsAdjacencyMatrix(self):      
        if self.adj_react_matrix is None:   
            self.getReactionMetaboliteNumbers() 

            adjAll = self.getAdjacencyMatrix()  
            matrix_size = self.number_reactions 
            adj_matrix = np.zeros((matrix_size, matrix_size))       

            for _, reaction_numberA in self.reaction_numbers.items():            
                metab_numbers = np.nonzero(adjAll[reaction_numberA, :]) 
                metab_numbers  = metab_numbers[0]  
                adjacent_reactions = np.nonzero(adjAll[metab_numbers,:])[1]  

                rA = reaction_numberA - self.number_metabolites 

                for reaction_numberB in adjacent_reactions:
                    rB = reaction_numberB - self.number_metabolites 
                    adj_matrix[rA, rB] += 1  

            self.adj_react_matrix = adj_matrix   
        return self.adj_react_matrix        


    #returns the list of all connections   
    def generateConnections(self, sM, fname):    
        pairs = np.nonzero(sM)  
        pairs = list(zip(pairs[0], pairs[1]))    
        with open(fname, 'a') as tsv_file: 
            np.savetxt(tsv_file, pairs, delimiter="\t",  fmt="%s")  

    #returns graphx from connection files  
    #constructs file if does not exists     
    def getGraphX(self):  
        #check if networkx object is not already initialized  
        if self.graph_x == None:

            #check if networx is not already serialized 
            fnamenx = os.path.join(self.biGG_path, self.biGG_model_name + "_all_reactions_nxobject.pickle")   
            if not os.path.isfile(fnamenx):
                fname = os.path.join(self.biGG_path, self.biGG_model_name + "_all_reactions.tsv")  

                if not os.path.isfile(fname):  
                    sM = self.getAdjacencyMatrix()    
                    self.generateConnections(sM, fname)     

                self.graph_x = nx.read_edgelist(fname, nodetype=int) 
                
                with open(fnamenx, "wb") as f:
                    pickle.dump(self.graph_x, f) 
            else:
                with open(fnamenx, "rb") as f:
                    self.graph_x = pickle.load(f)     

        return self.graph_x
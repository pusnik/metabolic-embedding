import json      
import numpy as np  
import matplotlib.pyplot as plt 
import cobra    
import h5py 

import networkx as nx    
import igraph as ig   
import generateBigGraphFiles as bigGraphsHelper

import os
import attr
import random
import torchbiggraph 
from torchbiggraph.converters.import_from_tsv import convert_input_data 
from torchbiggraph.config import parse_config
from torchbiggraph.train import train  
from torchbiggraph.eval import do_eval 
from get_network import Model  
from copy import copy, deepcopy 
from node2vec import Node2Vec 
import networkx as nx   
import time 
import pickle 

from gensim.models import Word2Vec 
from scipy.spatial import distance_matrix    

def getModelEmbedding(model, TRAIN_FRACTION = 0.95, num_epochs=128):    
    #split data  
    DATA_DIR = model.biGG_path 
    DATA_PATH = os.path.join(DATA_DIR, model.biGG_model_name + ".tsv") 
    CONFIG_PATH = "bigGraphConfig.py"
    FILENAMES = {
        'train': 'train.txt',
        'test': 'test.txt',
    } 

    bigGraphsHelper.random_split_file(DATA_PATH, FILENAMES, TRAIN_FRACTION)     
    edge_paths = [os.path.join(DATA_DIR, name) for name in FILENAMES.values()]

    train_config = parse_config(CONFIG_PATH, overrides=["entity_path="+DATA_DIR, "checkpoint_path="+DATA_DIR, "num_epochs="+str(num_epochs)]) 

    convert_input_data( 
        train_config.entities,  
        train_config.relations,  
        train_config.entity_path,  
        edge_paths,
        lhs_col=0,    
        rel_col=1,  
        rhs_col=2,   
    )  
    
    train_path = [bigGraphsHelper.convert_path(os.path.join(DATA_DIR, FILENAMES['train']))] 
    train_config = attr.evolve(train_config, edge_paths=train_path)
    train(train_config)   
                    
    eval_path = [bigGraphsHelper.convert_path(os.path.join(DATA_DIR, FILENAMES['test']))]
    eval_config = attr.evolve(train_config, edge_paths=eval_path)
    do_eval(eval_config)    


def getModel(modelName): 
    model = Model(biGG_model_name=modelName) 
    model.getBiGGModelDict()   
    return model

#return the powers of adjacency matrix
def getPows(adj, Km):
    powers = [adj]  
    for i in range(Km - 1):   
        powers.append(np.matmul(powers[i], adj))     
    return powers 

#transpose the powers of adjacency matrix   
def getTransposePows(pows):     
    t_pows = []
    for power in pows:
        t_pows.append(power.transpose())     
    return t_pows      

#returns the similarity based on the neigbours' degrees 
def getOutDegs(pows1, pows2, num_reactions1, num_reactions2, Km):
    S = np.zeros((num_reactions1, num_reactions2))  

    for pow1, pow2 in zip(pows1[:-1], pows2[:-1]):

        #count nonzero rows only once 
        countNonzero1 = np.count_nonzero(pow1, axis=1)  
        countNonzero2 = np.count_nonzero(pow2, axis=1)             

        degs1 = [] 
        degs2 = [] 

        i = 0
        #count total number of degrees of neigbours for both matrices 
        for row1 in pow1[-num_reactions1:]:
            #get indices of out neighbours  
            neigbs1 = np.nonzero(row1)[0]  
            #sum degrees of neigbours + degree of source origin node
            deg1 = np.sum(countNonzero1[neigbs1]) + (countNonzero1[-num_reactions1+i] if i == 0 else 0)    
            degs1.append(deg1)   
            i += 1    

        i = 0    
        #count total number of degrees of neigbours for both matrices 
        for row2 in pow2[-num_reactions2:]:  
            #get indices of out neighbours  
            neigbs2 = np.nonzero(row2)[0] 
            deg2 = np.sum(countNonzero2[neigbs2]) + (countNonzero2[-num_reactions2+i] if i == 0 else 0)               
            degs2.append(deg2)       
            i += 1 


        minS = np.minimum.outer(degs1, degs2)   
        maxS = np.maximum.outer(degs1, degs2).astype(float)   
        D =  np.divide(minS, maxS, out=np.ones_like(minS).astype(float), where=maxS!=0)    

        S = S + D 
    return (1.0/Km)*S        

#returns the similarity based on the number of vertices 
def getOutVerts(pows1, pows2, num_reactions1, num_reactions2):  
    neigs1 = [np.array([])]*num_reactions1 
    neigs2 = [np.array([])]*num_reactions2   

    Vs1 = np.zeros(num_reactions1)
    Vs2 = np.zeros(num_reactions2)

    for pow1, pow2 in zip(pows1, pows2):
        i = 0
        for row1 in pow1[-num_reactions1:]: 
            #perform union in order to avoid double counting of vertices  
            neigs1[i] = np.append(neigs1[i], np.nonzero(row1)[0])     
            i = i + 1   

        i = 0
        for row2 in pow2[-num_reactions2:]: 
            neigs2[i] = np.append(neigs2[i], np.nonzero(row2)[0]) 
            i = i + 1       

    #get only unique neigbours  
    neigs1 = list(map(np.unique, neigs1))        
    neigs2 = list(map(np.unique, neigs2))        

    Vs1 = list(map(len, neigs1))      
    Vs2 = list(map(len, neigs2))     

    Vs1 = np.array(Vs1) 
    Vs2 = np.array(Vs2)  
    #get maximums and minimums of all combinations   
    # if max=0 set quotient to 1   (divide where maxS != 0)    
    minS = np.minimum.outer(Vs1, Vs2)   
    maxS = np.maximum.outer(Vs1, Vs2).astype(float)   
    return np.divide(minS, maxS, out=np.ones_like(minS).astype(float), where=maxS!=0)  

#returns the similarity based on the number of edges  
def getOutEdgs(pows1, pows2, num_reactions1, num_reactions2, Km): 
    Vs1 = np.zeros(num_reactions1)
    Vs2 = np.zeros(num_reactions2) 

    #simply sum all edges in powers of adjacency matrices  
    for pow1, pow2 in zip(pows1, pows2):
        Vs1 = Vs1 + np.sum(pow1[-num_reactions1:], axis=1) 
        Vs2 = Vs2 + np.sum(pow2[-num_reactions2:], axis=1)     


    #get maximums and minimums of all combinations
    # if max=0 set quotient to 1     
    minS = np.minimum.outer(Vs1, Vs2)   
    maxS = np.maximum.outer(Vs1, Vs2).astype(float)   
    return np.divide(minS, maxS, out=np.ones_like(minS).astype(float), where=maxS!=0)  


#return similarity matrix from topology 
def getSTopology(model1, model2, Km):    
    #calculate the similarity matrix from metabolite reaction network   
    adj1 = model1.getAdjacencyMatrix()  
    adj2 = model2.getAdjacencyMatrix()  

    #get powers of adjacency matrix
    pows1 = getPows(adj1, Km)  
    pows2 = getPows(adj2, Km)

    t_pows1 = getTransposePows(pows1)    
    t_pows2 = getTransposePows(pows2)       

    #number of reactions in both networks, reactions come after metabolites  
    num_reactions1 = model1.number_reactions
    num_reactions2 = model2.number_reactions 

    S = np.zeros((num_reactions1, num_reactions2))       

    print("Calculating degrees")
    out_degs = getOutDegs(pows1, pows2, num_reactions1, num_reactions2, Km) 
    in_degs = getOutDegs(t_pows1, t_pows2, num_reactions1, num_reactions2, Km)     

    print("Calculating vertices")
    out_verts = getOutVerts(pows1, pows2, num_reactions1, num_reactions2)     
    in_verts = getOutVerts(t_pows1, t_pows2, num_reactions1, num_reactions2)       

    print("Calculating edges") 
    out_edgs = getOutEdgs(pows1, pows2, num_reactions1, num_reactions2, Km)    
    in_edgs = getOutEdgs(t_pows1, t_pows2, num_reactions1, num_reactions2, Km)          

    #topological similarity is the sum of all similarities
    S = S + out_degs + in_degs + out_edgs + in_edgs + out_verts + in_verts        
    return S    

#node2vec embedding with directed option enabled 
def getSNode2Vec(model1, model2, walk_length=10, num_walks=10, Km=3, epochs = 128):          
    #node dimension     
    dimension = 64          

    nx_G1 = model1.getGraphX()       
    nx_G2 = model2.getGraphX()     

    #number of workers for parallel execution   
    workers = 4 

    n2v1 = Node2Vec(nx_G1, dimensions=dimension, walk_length=walk_length, num_walks=num_walks, workers=workers)   
    n2v2 = Node2Vec(nx_G2, dimensions=dimension, walk_length=walk_length, num_walks=num_walks, workers=workers) 

    #fit both models while preserving all nodes, min_count = 1    
    n2vModel1 = n2v1.fit(window=Km, min_count=1, iter=epochs)
    n2vModel2 = n2v2.fit(window=Km, min_count=1, iter=epochs)    

    wv1 = n2vModel1.wv 
    wv2 = n2vModel2.wv  

    keys1 = map(str, range(model1.number_metabolites, model1.number_reactions + model1.number_metabolites)) 
    keys2 = map(str, range(model2.number_metabolites, model2.number_reactions + model2.number_metabolites))  

    dm1 = wv1[keys1] 
    dm2 = wv2[keys2]   

    dm1 = distance_matrix(dm1, dm1)     
    dm2 = distance_matrix(dm2, dm2)         

    if dm1.shape[1] > dm2.shape[1]:
        res = np.zeros((dm2.shape[0], dm1.shape[1])) 
        res[:dm2.shape[0], :dm2.shape[1]] = dm2 
        dm2 = res
    elif dm1.shape[1] < dm2.shape[1]:
        res = np.zeros((dm1.shape[0], dm2.shape[1]))  
        res[:dm1.shape[0], :dm1.shape[1]] = dm1
        dm1 = res

    print(dm1.shape)
    print(dm2.shape) 

    dm = distance_matrix(dm1, dm2)     
    return dm   

#torch big graph facebook embedding library  
def getSFacebookEmbedding(model1, model2, num_epochs=128): 
   
    model1.generateBigGraphFile() 
    model2.generateBigGraphFile() 


    getModelEmbedding(model1, num_epochs=num_epochs) 
    getModelEmbedding(model2, num_epochs=num_epochs)               

    with open(os.path.join(model1.biGG_path,"dictionary.json"), "rt") as tf:
        dictionary1 = json.load(tf)
        
    with open(os.path.join(model2.biGG_path,"dictionary.json"), "rt") as tf:
        dictionary2 = json.load(tf)        

    indices1 = []
    indices2 = []
    total1 = model1.number_metabolites + model1.number_reactions
    total2 = model2.number_metabolites + model2.number_reactions  
    reaction_offsets1 = dictionary1["entities"]["reaction_id"]  
    reaction_offsets2 = dictionary2["entities"]["reaction_id"]   
    for i in range(model1.number_metabolites, total1):   
        indices1.append(reaction_offsets1.index(str(i)))   
    for i in range(model2.number_metabolites, total2): 
        indices2.append(reaction_offsets2.index(str(i)))    

    embeddingFileReactions1 = os.path.join(model1.biGG_path, "embeddings_reaction_id_0.v" + str(num_epochs) + ".h5")  
    embeddingFileReactions2 = os.path.join(model2.biGG_path, "embeddings_reaction_id_0.v" + str(num_epochs) + ".h5")    


    #get model embeddings 
    with h5py.File(embeddingFileReactions1) as hf:
        reaction_embeddings1 = hf["embeddings"].value 

    with h5py.File(embeddingFileReactions2) as hf:
        reaction_embeddings2 = hf["embeddings"].value 

    dm1 = distance_matrix(reaction_embeddings1[indices1], reaction_embeddings1[indices1]) 
    dm2 = distance_matrix(reaction_embeddings2[indices2], reaction_embeddings2[indices2])  

    if dm1.shape[1] > dm2.shape[1]:
        res = np.zeros((dm2.shape[0], dm1.shape[1])) 
        res[:dm2.shape[0], :dm2.shape[1]] = dm2 
        dm2 = res
    elif dm1.shape[1] < dm2.shape[1]:
        res = np.zeros((dm1.shape[0], dm2.shape[1]))  
        res[:dm1.shape[0], :dm1.shape[1]] = dm1
        dm1 = res

    print(dm1.shape)
    print(dm2.shape) 

    dm = distance_matrix(dm1, dm2)     
    return dm 

#find source candidate nodes in the source pathway that are closer than Km to already aligned nodes 
def getSourceCandidateNodes(S, aligned1, powers1):   
    sourceCandidates = []  
    allSources = range(S.shape[0])  

    for a in aligned1:  
        allNeighbours = []    

        #go through all hops 
        for pow1 in powers1: 
            #find not aligned nodes in the Km neighborhoud of aligned nodes  
 
            notAlignedA = list(filter(lambda x: x not in aligned1, np.nonzero(pow1[a,:])[0])) #must be casted since filter returns iterable 
            notAlignedB = list(filter(lambda x: x not in aligned1, np.nonzero(pow1[:,a])[0])) 

            allNeighbours.extend(notAlignedA)  
            allNeighbours.extend(notAlignedB)    

        sourceCandidates.extend(allNeighbours)    

    if len(sourceCandidates) > 0: 
        #remove duplicates
        sourceCandidates = set(sourceCandidates)
        sourceCandidates = list(sourceCandidates)  
    else:
        #source candidates are all candidates that have not been aligned
        sourceCandidates = list(set(allSources) - set(aligned1))
    
    return sourceCandidates       

#return all possible projection  
def getProjections(indx, S):
    return np.nonzero(S[indx,:])[0]  

def getDirectNeighbours(indx, adjc):
    #out vertices   
    outN =  np.nonzero(adjc[indx,:])[0]
    #in vertices       
    inN = np.nonzero(adjc[:,indx])[0]    
    return np.unique(np.append(outN, inN))      

#check if i is direct neighbour to j 
def isNeighbour(i, j, adj):
    #this is faster tan calling getDirectNeighbours   
    return adj[i,j] != 0 or adj[j,i] != 0  

#get nodes that are in Km neigbourhood    
def getKM(i, pows):
    kmN = []  
    for powAdj in pows: 
        na = np.nonzero(powAdj[i,:])[0] 
        nb = np.nonzero(powAdj[:,i])[0] 
        kmN.extend(na)
        kmN.extend(nb) 
    
    return np.unique(kmN)    

#returns the alignment based on the similarity matrix S    
def getAlignment(S, adj1, adj2, Km):     
    #list of all alignment pairs  
    alignments = []  
    #list of aligned source nodes
    aligned1 = []    
    #list of aligned projection nodes  
    aligned2 = []  

    allSourceNodes = range(adj1.shape[0])   
    allDestinationNodes = range(adj2.shape[0])    

    #generate powers of adjacency matrices to get 1hop, 2hop, ... graphs  
    powers1 = getPows(adj1, Km)    
    powers2 = getPows(adj2, Km)    

    src_cand_nodes = np.array([])  

    #while S has nonzero elements 
    k = 0 
    while np.count_nonzero(S) > 0: 
        #find source candidate nodes if list contains zero elements    
        if src_cand_nodes.size == 0: 
            src_cand_nodes = np.array(getSourceCandidateNodes(S, aligned1, powers1))        

        #find the maximal similarity
        max_sim = np.max(S[src_cand_nodes])   
        i,j = np.argwhere(S[:,:] == max_sim)[0] 

        print(str(k) + ": " + str((i,j)))       
        k = k + 1  

        #remove aligned node from source candidate nodes 
        src_cand_nodes = np.delete(src_cand_nodes, np.argwhere(src_cand_nodes == i))   
        aligned1.append(i)    
        aligned2.append(j)       
        alignments.append([i,j])      

        src_cand_nodes = np.unique(np.append(src_cand_nodes, np.array(getSourceCandidateNodes(S, [i], powers1))))     

        #correct the similarity matrix 
        #set the row of the aligned source node to 0, the same source node cannot be projected more than once 
        S[i,:] = 0   
        #set the column of the aligned destionation node to 0
        S[:,j] = 0 

        #get direct neigbours of the aligned source node  
        #go through neighbour's projections and set to 0 if not in the Km neighbourhood of the aligned destination node j 
        #get km neigbourhood of destination source node j 
        neighbours = getDirectNeighbours(i, adj1)   
        kms = getKM(j, powers2)  
        notkms = np.setdiff1d(allDestinationNodes, kms) 

        ne = np.repeat(neighbours, len(notkms)) 
        nkms = np.tile(notkms, len(neighbours))  
        S[ne, nkms] = 0 
           
        #go thorugh nodes that are not in Km neighbourhood of node i
        #set projection to zero if not in Km of i and is neigbour of j 
        neighbours = getDirectNeighbours(j, adj2)  
        kms = getKM(i, powers1)    
        notKms = np.setdiff1d(allSourceNodes, kms)     

        nkms = np.repeat(notKms, len(neighbours)) 
        ne = np.tile(neighbours, len(notKms)) 
        S[nkms, ne] = 0       

    return alignments    

def getAccuracySameNetwork(alignments): 
	alignments = np.array(alignments)  
	count = alignments[:,0] == alignments[:,1]    
	nonzero = np.count_nonzero(count)     
	
	return nonzero/alignments.shape[0]

def coruptAdjacencyMatrix(adj):  
    randomM = np.random.choice([0,1,-1], size=adj.shape, p=[0.8,0.1,0.1])       
    adj = adj + randomM   
    return adj   

#main function 
if __name__ == "__main__":
    #neigbourhood size  
    Km = 3        
    
    #Big models 
    modelName1 = "iNJ661"      
    modelName2 = "iNJ661_2"                             

    model1 = getModel(modelName1)  
    model2 = getModel(modelName2)  

    adj1 = model1.getReactionsAdjacencyMatrix()       
    adj2 = model2.getReactionsAdjacencyMatrix() 
    adj2 = coruptAdjacencyMatrix(adj2)    

    #topological similarity  
    start = time.time() 
    s_mat = getSTopology(model1, model2, Km)              
    end = time.time()
    print("Elapsed time topology: " + str(end - start))  

    print(model1.reaction_numbers) 

    plt.matshow(s_mat)       
    plt.show()       

    alignments = getAlignment(s_mat, adj1, adj2, Km)    
    print("Topological alignment accuracy: " + str(getAccuracySameNetwork(alignments))) 
	#print("Alignments are: ")        
    #print(alignments)        


    #Node2Vec 
    start = time.time()    
    d_mat = getSNode2Vec(model1,model2, walk_length=Km, num_walks=256, Km=Km)
    end = time.time() 
    print("Elapsed time node2vec: " + str(end - start))    

    plt.matshow(1.0/(d_mat))    
    plt.show()     

    alignments = getAlignment(1.0/d_mat, adj1, adj2, Km) 
    print("node2vec alignment accuracy: " + str(getAccuracySameNetwork(alignments))) 
	#print("Alignments are: ")    
    #print(alignments) 

    #Facebook BigGraph  
    start = time.time() 
    d_mat = getSFacebookEmbedding(model1, model2) 
    end = time.time()
    print("Elapsed time biggraph: " + str(end - start))                   
    print(d_mat.shape)     

    plt.matshow(1.0/d_mat)    
    plt.show()     

    alignments = getAlignment(1.0/d_mat, adj1, adj2, Km) 
    print("biggraph alignment accuracy: " + str(getAccuracySameNetwork(alignments)))     
	#print("Alignments are: ")   
    #print(alignments)       